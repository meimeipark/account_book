package com.pym.accountbookapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AccountMoneyType {
    STATIC_INPUT("고정수입", false),
    DHCP_INPUT("변동수입", false),
    STATIC_OUTPUT("고정지출", true),
    DHCP_OUTPUT("변동지출", true);

    private final String name;
    private final Boolean isMoneyOutput;
}
