package com.pym.accountbookapi.controller;

import com.pym.accountbookapi.model.AccountBookItem;
import com.pym.accountbookapi.model.AccountBookRequest;
import com.pym.accountbookapi.model.AccountBookResponse;
import com.pym.accountbookapi.service.AccountBookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/account")
public class AccountBookController {
    private final AccountBookService accountBookService;

    @PostMapping("/new")
    public String setAccountBook(@RequestBody AccountBookRequest request){
        accountBookService.setAccountBook(request);

        return "등록완료";
    }

    @GetMapping("/all")
    public List<AccountBookItem> getAccountBooks(){
        return accountBookService.getAccountBooks();
    }

    @GetMapping("/detail/{id}")
    public AccountBookResponse getAccountBook(@PathVariable long id){
        return accountBookService.getAccountBook(id);
    }
}
