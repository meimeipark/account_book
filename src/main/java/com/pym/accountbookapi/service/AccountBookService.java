package com.pym.accountbookapi.service;

import com.pym.accountbookapi.entity.AccountBook;
import com.pym.accountbookapi.model.AccountBookItem;
import com.pym.accountbookapi.model.AccountBookRequest;
import com.pym.accountbookapi.model.AccountBookResponse;
import com.pym.accountbookapi.repository.AccountBookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountBookService {
    private final AccountBookRepository accountBookRepository;

    public void setAccountBook(AccountBookRequest request){
        AccountBook addData = new AccountBook();
        addData.setDateCreate(LocalDate.now());
        addData.setAccountMoneyType(request.getAccountMoneyType());
        addData.setMoney(request.getMoney());
        addData.setCategory(request.getCategory());
        addData.setEtcMemo(request.getEtcMemo());

        accountBookRepository.save(addData);
    }

    public List<AccountBookItem> getAccountBooks(){
        List<AccountBook> originList = accountBookRepository.findAll();

        List<AccountBookItem> result = new LinkedList<>();

        for (AccountBook accountBook: originList){
            AccountBookItem addItem = new AccountBookItem();
            addItem.setId(accountBook.getId());
            addItem.setDateCreate(accountBook.getDateCreate());
            addItem.setAccountMoneyType(accountBook.getAccountMoneyType().getName());
            addItem.setIsMoneyOutput(accountBook.getAccountMoneyType().getIsMoneyOutput());
            addItem.setMoney(accountBook.getMoney());
            addItem.setCategory(accountBook.getCategory());

            result.add(addItem);
        }
        return result;
    }

    public AccountBookResponse getAccountBook(long id){
        AccountBook originData = accountBookRepository.findById(id).orElseThrow();

        AccountBookResponse response = new AccountBookResponse();
        response.setId(originData.getId());
        response.setDateCreate(originData.getDateCreate());
        response.setAccountMoneyTypeName(originData.getAccountMoneyType().getName());
        response.setIsMoneyOutputType(originData.getAccountMoneyType().getIsMoneyOutput() ? "지출":"수입");
        response.setMoney(originData.getMoney());
        response.setCategory(originData.getCategory());

        return response;
    }
}
