package com.pym.accountbookapi.repository;

import com.pym.accountbookapi.entity.AccountBook;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountBookRepository extends JpaRepository <AccountBook, Long> {
}
