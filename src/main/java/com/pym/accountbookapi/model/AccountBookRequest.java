package com.pym.accountbookapi.model;

import com.pym.accountbookapi.enums.AccountMoneyType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountBookRequest {
    @Enumerated(value = EnumType.STRING)
    private AccountMoneyType accountMoneyType;
    private Long money;
    private String category;
    private String etcMemo;
}
