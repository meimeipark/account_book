package com.pym.accountbookapi.model;

import com.pym.accountbookapi.enums.AccountMoneyType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AccountBookItem {
    private Long id;
    private LocalDate dateCreate;
    private String accountMoneyType;
    private Boolean isMoneyOutput;
    private Long money;
    private String category;
}
