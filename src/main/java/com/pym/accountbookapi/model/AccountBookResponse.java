package com.pym.accountbookapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AccountBookResponse {
    private Long id;
    private LocalDate dateCreate;
    private String accountMoneyTypeName;
    private String isMoneyOutputType;
    private Long money;
    private String category;
    private String etcMemo;
}
