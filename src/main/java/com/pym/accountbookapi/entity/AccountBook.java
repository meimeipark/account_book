package com.pym.accountbookapi.entity;

import com.pym.accountbookapi.enums.AccountMoneyType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class AccountBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private AccountMoneyType accountMoneyType;

    @Column(nullable = false)
    private Long money;

    @Column(nullable = false, length = 30)
    private String category;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
